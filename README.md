# ИНСТРУКЦИЯ

# Зависимости
- https://nodejs.org/en/
- docker
- docker-compose
- kubectl
- helm
- локальный k8s

# Локальный запуск только для разработки
- npm i
- npm run dev:env - дождаться запуска контейнеров
- npm run dev:start - запуск всех процессов
- http://localhost:9000/

# Задачи и требования
- докеризировать проект, нужны поды front, api, worker
- запушить образы в https://hub.docker.com/ (регистрация под любой учеткой)
- создать helm chart
- реализовать readinessProbe для api и worker (GET /healthcheck = HTTP 200)
- helm должен запускаться с аргументом --wait и ждать успешный запуск всего окружения
